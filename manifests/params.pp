# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include pihole_exporter::params
class pihole_exporter::params {

  $manage_user      = true
  $manage_group     = true
  $user             = 'pihole_exporter'
  $group            = 'pihole_exporter'
  $version          = '0.0.6'

  case $::architecture {
    'x86_64', 'amd64': { $arch = 'amd64' }
    'i386':            { $arch = '386' }
    default: {
      fail("${::architecture} unsuported")
    }
  }

  case $::kernel {
    'Linux': { $os = downcase($::kernel)}
    default: {
      fail("${::kernel} not supported")
    }
  }

  $archive_url      = "https://github.com/eko/pihole-exporter/releases/download/${version}/pihole_exporter-${os}-${arch}"
  $archive_checksum = '35a224b8f6c493de4695669dd38b5ca2a5b65b04d4b56c2a3d5a5a2ee7af3966'

  $admin_url         = '127.0.0.1'
  $admin_password    = undef
}
