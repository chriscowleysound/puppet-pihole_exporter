
# pihole exporter

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with pihole_exporter](#setup)
    * [What pihole_exporter affects](#what-pihile_exporter-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with pihole_exporter](#beginning-with-pihile_exporter)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs the Prometheus Pi-hole Exporter.

## Setup

### What pihole_exporter affects **OPTIONAL**

Installs the release from Github and creates a Systemd unit file to run the service under a dedicated user.


### Beginning with pihole_exporter  

```
node '<pihole-node>' {
  include ::pihole_exporter
}
```

This will monitor the Pi Hole on `localhost` which should be fine for and sane system. For now there are no parameters to control the configuration as I do not need them.

## Limitations

Tested on Centos 7, but should be fine on any Linux that uses Systemd.

## Development

Create a Merge Request
