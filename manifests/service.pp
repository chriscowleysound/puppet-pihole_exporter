# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include pihole_exporter::service
class pihole_exporter::service {
  file {'/etc/systemd/system/pihole_exporter.service':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp('pihole_exporter/pihole_exporter.systemd.epp')
  }
}
