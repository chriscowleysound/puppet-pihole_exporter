# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include pihole_exporter
class pihole_exporter (
  Optional[String] $version = $::pihole_exporter::params::version,
  Optional[String] $archive_url = $::pihole_exporter::params::archive_url,
  Optional[String] $archive_checksum = $::pihole_exporter::params::archive_checksum,
  Optional[Boolean] $manage_user      = $::pihole_exporter::params::manage_user,
  Optional[Boolean] $manage_group     = $::pihole_exporter::params::manage_group,
  Optional[String] $user             = $::pihole_exporter::params::user,
  Optional[String] $group            = $::pihole_exporter::params::group,
  Optional[String] $admin_url        = $::pihole_exporter::params::admin_url,
  String $admin_password             = $::pihole_exporter::params::admin_password

) inherits ::pihole_exporter::params {
  class { '::pihole_exporter::install': }
  -> class{ '::pihole_exporter::config': }
  ~> class{ '::pihole_exporter::service': }
  -> Class['::pihole_exporter']
}
