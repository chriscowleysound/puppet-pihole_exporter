# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include pihole_exporter::install
class pihole_exporter::install {
  if $::pihole_exporter::manage_user {
    ensure_resource('user', [$::pihole_exporter::user], {
      ensure => 'present',
      system => true,
    })
  }
  if $::pihole_exporter::manage_group {
    ensure_resource('group', [$::pihole_exporter::group], {
      ensure => 'present',
      system => true,
    })
    Group[$::pihole_exporter::group]->User[$::pihole_exporter::user]
  }
  remote_file { '/usr/local/bin/pihole_exporter':
    ensure        => present,
    source        => $::pihole_exporter::archive_url,
    checksum      => $::pihole_exporter::archive_checksum,
    checksum_type => 'sha256',
    owner         => $::pihole_exporter::user,
    group         => $::pihole_exporter::group,
    mode          => '0755',
  }
}
